'''
                                 ESP Health
                        Notifiable Diseases Framework
                Diagnosed and Controlled Diabetes Case Generator


@author: Jeff Andre <jandre@commoninf.com>
@organization: commonwealth informatics https://www.commoninf.com
@contact: https://www.esphealth.org
@copyright: (c) 2020 Commonwealth Informatics, Inc.
@license: LGPL
'''

import datetime
from decimal import Decimal
from django.db.models import F

from ESP.utils import log
from ESP.hef.base import Event
from ESP.hef.base import DiagnosisHeuristic
from ESP.hef.base import LabResultFixedThresholdHeuristic, LabResultRangeHeuristic
from ESP.hef.base import Dx_CodeQuery
from ESP.nodis.base import DiseaseDefinition
from ESP.nodis.models import Case, CaseActiveHistory
from ESP.conf.models import LabTestMap
from ESP.utils.utils import queryset_iterator
from ESP.emr.models import Encounter, Patient

class DiabetesDiagnosisHeuristic(DiagnosisHeuristic):
    '''Generates first and only dx event for patients with diagnosed-diabetes of
       ages between 18 and 75 years on the encounter(visit) date.
    '''

    def __init__(self, min_dob, max_dob, *args, **kwargs):
        super(DiabetesDiagnosisHeuristic, self).__init__(*args, **kwargs)

        self.min_dob = min_dob
        self.max_dob = max_dob

    def generate(self):
        '''Override generate method.
        '''

        enc_qs = self.encounters
        enc_qs = enc_qs.exclude(events__name__in=self.event_names)
        # only include patients of ages 18-75 years
        enc_qs = enc_qs.filter(patient__date_of_birth__lte = F('date') - self.min_dob,
                               patient__date_of_birth__gte = F('date') - self.max_dob,
                            )
        enc_qs = enc_qs.order_by('date').distinct()

        # generate dx events
        counter = 0
        for enc in enc_qs:

            # gestational diabetes events within one year
            dx_gdm_events = Event.objects.filter(patient=enc.patient,
                                                 name='dx:gestational-diabetes',
                                                 date__gte = enc.date - datetime.timedelta(days=365),
                                                 date__lte = enc.date + datetime.timedelta(days=365) )
            # diagnosed diabetes event
            patient_dx_event = Event.objects.filter(patient=enc.patient, 
                                                    name=self.dx_event_name,)

            if not patient_dx_event and not dx_gdm_events:
                Event.create(
                    name = self.dx_event_name,
                    source = self.uri,
                    patient = enc.patient,
                    date = enc.date,
                    provider = enc.provider,
                    emr_record = enc,
                    )
                counter += 1
        return counter

class DiagnosedDiabetes(DiseaseDefinition):
    '''
    Diagnosed and Controlled Diabetes
    '''
    
    conditions = ['diabetes:diagnosed']
    
    uri = 'urn:x-esphealth:disease:channing:diagnosed-diabetes:v1'
    
    short_name = 'diag_diab'
    
    test_name_search_strings = ['a1c','diab']

    timespan_heuristics = []
    
    # dates
    max_measure_date = datetime.datetime.now() - datetime.timedelta(days=365)
    min_birth_date = datetime.timedelta(days=18*365)
    max_birth_date = datetime.timedelta(days=75*365)

    
    # event names
    ct_name = ['lx:a1c:threshold:lt:7']
    sc_name = ['lx:a1c:range:gte:7:lt:8']
    uc_name = ['lx:a1c:range:gte:8:lte:9']
    su_name = ['lx:a1c:threshold:gt:9']
    dx_event_name = ['dx:diabetes:diagnosed']

    @property
    def event_heuristics(self):
        heuristic_list = []
        
        # Diagnosis Codes
        heuristic_list.append( DiabetesDiagnosisHeuristic(
            name = 'diabetes:diagnosed',
            dx_code_queries = [
                Dx_CodeQuery(starts_with='250.', type='icd9'),
                Dx_CodeQuery(starts_with='249.01', type='icd9'),
                Dx_CodeQuery(starts_with='249.1', type='icd9'),
                Dx_CodeQuery(starts_with='249.2', type='icd9'),
                Dx_CodeQuery(starts_with='249.3', type='icd9'),
                Dx_CodeQuery(starts_with='249.4', type='icd9'),
                Dx_CodeQuery(starts_with='249.5', type='icd9'),
                Dx_CodeQuery(starts_with='249.6', type='icd9'),
                Dx_CodeQuery(starts_with='249.7', type='icd9'),
                Dx_CodeQuery(starts_with='249.8', type='icd9'),
                Dx_CodeQuery(starts_with='249.9', type='icd9'),
                Dx_CodeQuery(starts_with='E10', type='icd10'),
                Dx_CodeQuery(starts_with='E11', type='icd10'),
                Dx_CodeQuery(starts_with='E13', type='icd10'),
                ],
            min_dob = self.min_birth_date,
            max_dob = self.max_birth_date,
            ))

        # Labs
        # Controlled (a1c)
        heuristic_list.append(LabResultFixedThresholdHeuristic(
                test_name = 'a1c',
                threshold = Decimal(str(7)),
                match_type = 'lt',))
        # Semi-Controlled (a1c)
        heuristic_list.append(LabResultRangeHeuristic(
                test_name = 'a1c',
                min = Decimal(str(7)),
                max = Decimal(str(8)),
                min_match = 'gte',
                max_match = 'lt',))
        # Uncontrolled (a1c)
        heuristic_list.append(LabResultRangeHeuristic(
                test_name = 'a1c',
                min = Decimal(str(8)),
                max = Decimal(str(9)),
                min_match = 'gte',
                max_match = 'lte',))
        # Severely Uncontrolled (a1c)
        heuristic_list.append(LabResultFixedThresholdHeuristic(
                test_name = 'a1c',
                threshold = Decimal(str(9)),
                match_type = 'gt',))

        return heuristic_list

    def a1cStatus(self, a1c):
        
        status = 'NONE'
        if a1c.name in self.ct_name:
            status = "DIAB-C" # controlled
        elif a1c.name in self.sc_name:
            status = "DIAB-SC" # semi-controlled
        elif a1c.name in self.uc_name:
            status = "DIAB-U" # uncontrolled
        elif a1c.name in self.su_name:
            status = "DIAB-SU" # severely uncontrolled
        return status

    def updateCaseActiveHistory(self, this_event, this_case):
        '''Updates CaseActiveHistory with new a1c lab events and exlclude patients
           older than 75 years.
        '''

        # return False if excluded
        last_cah = CaseActiveHistory.objects.filter(case=this_case).last()
        excluded = last_cah and last_cah.status == 'DIAB-E'
        if excluded: 
            return False

        # get A1C lab events
        #a1c_event_names = self.wct_name + self.ct_name + self.uct_name
        a1c_event_names = self.ct_name + self.sc_name + self.uc_name + self.su_name
        a1c_events = Event.objects.filter(patient=this_event.patient,
                                        date__gte=(this_event.date - datetime.timedelta(days=30)),
                                        name__in=a1c_event_names).order_by('date')


	# update CAH with a1c events
        last_a1c = None
        if a1c_events:
            for a1c in a1c_events:
                # new CAH
                last_cah = CaseActiveHistory.objects.filter(case=this_case).last()
                a1c_start_date = a1c.date >= (this_event.date - datetime.timedelta(days=30))
                a1c_end_date = a1c.date <= this_event.date
                valid_a1c_before_dx = a1c_start_date and a1c_end_date
                if not last_cah and valid_a1c_before_dx: 
                    # new CAH for a1c within 30 days before or on dx event date
                    a1c_status = self.a1cStatus(a1c)
                    CaseActiveHistory.create(
                        case = this_case,
                        status = a1c_status,
                        date = this_event.date,
                        change_reason = "DIAB-NEW",
                        event_rec = this_event,
		    )
                    this_case.events.add(a1c)
                elif not last_cah:
                    # new CAH not measured
                    CaseActiveHistory.create(
                        case = this_case,
                        status = "DIAB-N",
                        date = this_event.date,
                        change_reason = "DIAB-NEW",
                        event_rec=this_event,
                        )

                # update CAH with a1c 
                last_cah = CaseActiveHistory.objects.filter(case=this_case).last()
                update_cah = last_cah and (a1c.date > last_cah.date)
                if last_cah and (a1c.date > last_cah.date): 
                    a1c_status = self.a1cStatus(a1c)
                    CaseActiveHistory.create(
                        case = this_case,
                        status = a1c_status,
                        date = a1c.date,
                        change_reason = "DIAB-UPD",
                        event_rec = a1c,
		    )
                    this_case.events.add(a1c)

	    # check if un-measured a1c (last a1c more than a year ago from today)
            last_a1c = a1c_events.last()
            last_a1c_expired = last_a1c.date < self.max_measure_date.date()
            last_cah = CaseActiveHistory.objects.filter(case=this_case).last()
            unmeasured = last_cah and last_cah.status =='DIAB-N'
            if last_a1c_expired and last_cah and not unmeasured:
                cah_date = last_a1c.date + datetime.timedelta(days=365)
                CaseActiveHistory.create(
                        case = this_case,
                        status = "DIAB-N",
                        date = cah_date,
                        change_reason = "DIAB-UPD",
                        event_rec = last_a1c,
                )

        # new CAH for dx event with no a1c 
        elif not last_cah:
            CaseActiveHistory.create(
                case = this_case,
                status = "DIAB-N",
                date = this_event.date,
                change_reason = "DIAB-NEW",
                event_rec=this_event,
                )

        # exclude patients older than 75 years
        today = datetime.datetime.today()
        dob_cutoff = today - datetime.timedelta(days=75*365)
        use_last_a1c = a1c_events and last_a1c and last_a1c.date > this_event.date
        last_event = last_a1c if use_last_a1c else this_event
        if (this_event.patient.date_of_birth is not None) and (this_event.patient.date_of_birth < dob_cutoff):
            CaseActiveHistory.create(
                case = this_case,
                status = 'DIAB-E', # exclude
                date = today,
                change_reason = "DIAB-UPD",
                event_rec = last_event,
	    )

    def createCase(self, dx_event):
        '''Creates diagnosed-diabetes case for dx_event and creates CaseActiveHistory
        Returns new case
        '''

	# create case
        case_criteria = 'Criteria: Diagnosed and controlled diabetes patient between ages of 18 and 75, excluding gestational and sterioid induced diabetes'
        new_case = Case(
                    condition = self.conditions[0],
                    patient = dx_event.patient,
                    provider = dx_event.provider,
                    date = dx_event.date,
                    criteria = case_criteria,
                    source = self.uri,
            )
        new_case.save()
        new_case.events.add(dx_event)
        return new_case

    def _generate_cases(self, dx_event_qs):
        """Generates diagnosed and controlled diabetes cases.
           Returns number of genereated cases.
        """

        counter = 0
        for this_event in dx_event_qs:

            # get existing case for diagnosed diabetes
            this_case = Case.objects.filter(
                    condition=self.conditions[0],
                    patient=this_event.patient,
                    ).last()

	    # or create new case
            if not this_case:
                this_case = self.createCase(this_event)
                log.debug('Created new diagnosed diabetes case: %s' % this_case)
                counter += 1
			
	    # update CaseActiveHistory with A1C labs
            self.updateCaseActiveHistory(this_event, this_case)


        log.debug('Generating %s new cases of diagnosed diabetes' % counter)
        return counter # count of new cases

    def generate(self):
        """ Generate cases for patients between ages of 18 and 75 with dx codes for diabetes, 
            excluding gestational and sterioid induced diabetes.
        """
        log.info('Generating cases of %s' % self.short_name)

        # get all diagnosed diabetes events
        dx_event_qs = Event.objects.filter(
            name__in = self.dx_event_name,
        ).order_by('date')

        # generate cases
        case_count = 0
        if dx_event_qs:
            case_count = self._generate_cases(dx_event_qs)
        
        return case_count

#-------------------------------------------------------------------------------
#
# Packaging
#
#-------------------------------------------------------------------------------

def event_heuristics():
    return DiagnosedDiabetes().event_heuristics

def disease_definitions():
    return [DiagnosedDiabetes()]
